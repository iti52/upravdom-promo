/**
 * Created on 16.12.16.
 */

jQuery(document).ready(function() {
    jQuery('#main-carousel').flickity({
        cellAlign: 'left',
        contain: true,
        prevNextButtons: false,
        arrowShape: {
            x0: 10,
            x1: 60, y1: 50,
            x2: 60, y2: 45,
            x3: 15
        }
    });
    jQuery('.faq-item .answer').hide();
    jQuery('.question span').click(function() {
        jQuery(this).parent().toggleClass('open');
        jQuery(this).parent().parent().find('.answer').toggle(300);
    });
});